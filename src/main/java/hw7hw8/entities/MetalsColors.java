package hw7hw8.entities;

import com.epam.jdi.tools.DataClass;

import java.util.List;

// TODO naming - FIXED
public class MetalsColors extends DataClass<MetalsColors> {
    public String summaryOdd, summaryEven;
    public List<String> elements;
    public String colors;
    public String metals;
    public List<String> vegetables;
}
