package hw7hw8;

import com.epam.jdi.light.elements.pageobjects.annotations.JSite;
import hw7hw8.pages.HomePage;
import hw7hw8.pages.MetalsColorsPage;

@JSite("https://epam.github.io/JDI")
public class JdiTestSite {
    public static HomePage homePage;
    public static MetalsColorsPage metalsColorsPage;
}
