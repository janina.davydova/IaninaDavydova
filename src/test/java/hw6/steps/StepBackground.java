package hw6.steps;

import hw3.voids.DifferentElementsPage;
import hw4.HomePage;
import hw6.UserTablePage;

public abstract class StepBackground {
    static HomePage homePage;

    static DifferentElementsPage differentElementsPage;
    static UserTablePage userTablePage;
}
